﻿using FactoryPattern.AbstractFactory.Drinks;
using FactoryPattern.AbstractFactory.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.AbstractFactory.DrinkFractories
{
    internal class CoffeeFactory : IHotDrinkFactory
    {
        public IHotDrink Prepare(int amount)
        {
            Console.WriteLine($"Grind some beans, boil water, pour {amount} ml, add cream and sugar, enjoy!");
            return new Coffee();
        }
    }
}
