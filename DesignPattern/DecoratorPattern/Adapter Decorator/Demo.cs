﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace DecoratorPattern.Adapter_Decorator
{
    public class Demo
    {
        static void Main(string[] args)
        {
            MyStringBuilder s = "hello ";
            s += "world"; // will work even without op+ in MyStringBuilder
                          // why? you figure it out!
            WriteLine(s);
        }
    }
}
