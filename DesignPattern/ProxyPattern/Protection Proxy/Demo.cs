﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Protection_Proxy
{
    public class Demo
    {
        static void Main(string[] args)
        {
            ICar car = new CarProxy(new Driver(12)); // 22
            car.Drive();
        }
    }
}
