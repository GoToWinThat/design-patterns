﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace DecoratorPattern.CodeBuilder
{
    public class Demo
    {
        static void Main(string[] args)
        {
            var cb = new CodeBuilder();
            cb.AppendLine("class Foo")
              .AppendLine("{")
              .AppendLine("}");
            WriteLine(cb);
        }
    }
}
