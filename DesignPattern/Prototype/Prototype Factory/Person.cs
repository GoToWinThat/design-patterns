﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Prototype_Factory
{
    public partial class Person
    {
        public string Name;
        public Address Address;

        public Person(string name, Address address)
        {
            Name = name ?? throw new ArgumentNullException(paramName: nameof(name));
            Address = address ?? throw new ArgumentNullException(paramName: nameof(address));
        }

        public Person(Person other)
        {
            Name = other.Name;
            Address = new Address(other.Address);
        }

        public override string ToString()
        {
            return $"{nameof(Name)}: {Name}, {nameof(Address)}: {Address}";
        }

        //partial class EmployeeFactory {}
    }
}
