﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompositePattern.Basic_Composite
{
    public class Circle : GraphicObject
    {
        public override string Name => "Circle";
    }
}
