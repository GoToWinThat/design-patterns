﻿using static System.Console;
using System.Collections.Generic;
using System.Text;

namespace BridgePattern.Bridge
{
    public class RasterRenderer : IRenderer
    {
        public void RenderCircle(float radius)
        {
            WriteLine($"Drawing pixels for circle of radius {radius}");
        }
    }
}
