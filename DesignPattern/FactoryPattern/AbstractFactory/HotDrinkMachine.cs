﻿using FactoryPattern.AbstractFactory.DrinkFractories;
using FactoryPattern.AbstractFactory.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.AbstractFactory
{
    public class HotDrinkMachine
    {


        private List<Tuple<string, IHotDrinkFactory>> namedFactories = new List<Tuple<string, IHotDrinkFactory>>();

        public HotDrinkMachine()
        {
            //Searchin all types of drinks
            foreach (var t in typeof(HotDrinkMachine).Assembly.GetTypes())
            {
                //Flag if drink class have interface 
                if (typeof(IHotDrinkFactory).IsAssignableFrom(t) && !t.IsInterface)
                {
                    //Saving type
                    namedFactories
                        .Add(
                            Tuple
                                .Create(
                                t.Name.Replace("Factory", string.Empty), 
                                (IHotDrinkFactory)Activator.CreateInstance(t))
                                );
                }
            }
        }

        public IHotDrink MakeDrink(string type)
        {
            switch (type)
            {
                case "tea":
                    return new TeaFactory().Prepare(200);
                case "coffee":
                    return new CoffeeFactory().Prepare(50);
                default:
                    throw new ArgumentException("type");
            }
        }

        public IHotDrink MakeDrink()
        {
            Console.WriteLine("Available drinks");
            for (var index = 0; index < namedFactories.Count; index++)
            {
                var tuple = namedFactories[index];
                Console.WriteLine($"{index}: {tuple.Item1}");

            }

            while (true)
            {
                string s;
                if (
                    (s = Console.ReadLine()) != null
                    && int.TryParse(s, out int i)
                    && i >= 0
                    && i < namedFactories.Count)
                {
                    Console.Write("Specify amount: ");
                    s = Console.ReadLine();
                    if (s != null
                        && int.TryParse(s, out int amount)
                        && amount > 0)
                    {
                        return namedFactories[i].Item2.Prepare(amount);
                    }
                }
                Console.WriteLine("Incorrect input, try again.");
            }
        }
    }
}
