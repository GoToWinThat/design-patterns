﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern.Inheritance_Decorator
{
    public interface ICreature
    {
        int Age { get; set; }
    }
}
