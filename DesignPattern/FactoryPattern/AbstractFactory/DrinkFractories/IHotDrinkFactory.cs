﻿using FactoryPattern.AbstractFactory.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.AbstractFactory.DrinkFractories
{
    public interface IHotDrinkFactory
    {
        IHotDrink Prepare(int amount);
    }
}
