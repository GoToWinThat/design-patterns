﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern.Basic_Decorator
{
    public class ColoredShape : Shape
    {
        private readonly Shape shape;
        private readonly string color;

        public ColoredShape(Shape shape, string color)
        {
            this.shape = shape;
            this.color = color;
        }

        public override string AsString() => $"{shape.AsString()} has the color {color}";
    }

    public class ColoredShape<T> : Shape
  where T : Shape, new()
    {
        private readonly string color;
        private readonly T shape = new T();

        public ColoredShape() : this("black")
        {

        }

        public ColoredShape(string color) // no constructor forwarding
        {
            this.color = color;
        }

        public override string AsString()
        {
            return $"{shape.AsString()} has the color {color}";
        }
    }
}
