﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ChainOfResposibilityPattern.Method_Chain
{

    public class IncreaseDefenseModifier : CreatureModifier
    {
        public IncreaseDefenseModifier(Creature creature)
          : base(creature) { }

        public override void Handle()
        {
            if (creature.Attack <= 2)
            {
                WriteLine($"Increasing {creature.Name}'s defense");
                creature.Defense++;
            }

            base.Handle();
        }
    }
}
