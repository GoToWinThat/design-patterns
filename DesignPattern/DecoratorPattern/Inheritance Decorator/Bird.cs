﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace DecoratorPattern.Inheritance_Decorator
{
    public class Bird : IBird
    {
        public int Age { get; set; }
        public void Fly()
        {
            if (Age >= 10)
                WriteLine("I am flying!");
        }
    }
}
