﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;
namespace ProxyPattern.Protection_Proxy
{
    public class CarProxy : ICar
    {
        private Car car = new Car();
        private Driver driver;

        public CarProxy(Driver driver)
        {
            this.driver = driver;
        }

        public void Drive()
        {
            if (driver.Age >= 16)
                car.Drive();
            else
            {
                WriteLine("Driver too young");
            }
        }
    }
}
