﻿using Autofac;
using System;
using System.Collections.Generic;
using System.Text;

namespace BridgePattern.Bridge
{
    public class Demo
    {
        static void Main(string[] args)
        {
            var raster = new RasterRenderer();
            var vector = new VectorRenderer();
            var circle = new Circle(vector, 5);
            circle.Draw();
            circle.Resize(2);
            circle.Draw();

            var cb = new ContainerBuilder();
            cb.RegisterType<VectorRenderer>().As<IRenderer>();
            cb.Register((c, p) => new Circle(c.Resolve<IRenderer>(),
              p.Positional<float>(0)));
            using (var c = cb.Build())
            {
                var ccircle = c.Resolve<Circle>(
                  new PositionalParameter(0, 5.0f)
                );
                ccircle.Draw();
                ccircle.Resize(2);
                ccircle.Draw();
            }
        }
    }
}
