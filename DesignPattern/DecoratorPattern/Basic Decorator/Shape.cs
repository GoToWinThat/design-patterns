﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern.Basic_Decorator
{
    public abstract class Shape
    {
        public virtual string AsString() => string.Empty;
    }
}
