﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Protection_Proxy
{
    public interface ICar
    {
        void Drive();
    }
}
