﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FlyweightPattern.UserExample
{
    public class FirstUser
    {
        public string FullName { get; }

        public FirstUser(string fullName)
        {
            FullName = fullName;
        }
    }
}
