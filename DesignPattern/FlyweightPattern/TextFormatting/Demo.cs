﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;
namespace FlyweightPattern.TextFormatting
{
    public class Demo
    {
        static void Main(string[] args)
        {
            var ft = new FormattedText("This is a brave new world");
            ft.Capitalize(10, 15);
            WriteLine(ft);

            var bft = new BetterFormattedText("This is a brave new world");
            bft.GetRange(10, 15).Capitalize = true;
            WriteLine(bft);
        }
    }
}
