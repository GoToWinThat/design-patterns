﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AdapterPattern.Basic_Adapter
{
    public class Line
    {
        public Point Start, End;

        public Line(Point start, Point end)
        {
            Start = start;
            End = end;
        }
    }
}
