﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;
namespace ProxyPattern.Virtual_Proxy
{
    class Bitmap : IImage
    {
        private readonly string filename;

        public Bitmap(string filename)
        {
            this.filename = filename;
            WriteLine($"Loading image from {filename}");
        }

        public void Draw()
        {
            WriteLine($"Drawing image {filename}");
        }
    }
}
