﻿using FactoryPattern.AbstractFactory.Drinks;
using FactoryPattern.AbstractFactory.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.AbstractFactory.DrinkFractories
{
    internal class TeaFactory : IHotDrinkFactory
    {
        public IHotDrink Prepare(int amount)
        {
            Console.WriteLine($"Put in tea bag, boil water, pour {amount} ml, add lemon, enjoy!");
            return new Tea();
        }
    }
}
