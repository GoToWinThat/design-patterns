﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern.Basic_Decorator
{
    public class TransparentShape : Shape
    {
        private readonly Shape shape;
        private readonly float transparency;

        public TransparentShape(Shape shape, float transparency)
        {
            this.shape = shape;
            this.transparency = transparency;
        }

        public override string AsString() =>
          $"{shape.AsString()} has {transparency * 100.0f}% transparency";
    }

    public class TransparentShape<T> : Shape where T : Shape, new()
    {
        private readonly float transparency;
        private readonly T shape = new T();

        public TransparentShape(float transparency)
        {
            this.transparency = transparency;
        }

        public override string AsString()
        {
            return $"{shape.AsString()} has transparency {transparency * 100.0f}";
        }
    }
}
