﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Basic_Factory
{
    public partial class Point
    {
        //Private factory - this factor can use private properties
        // .NET example is factory in task class: Task.Factory.Startnew();
        public static class Factory
        {
            public static Point NewCartesianPoint(double x, double y)
            {
                return new Point(x, y);
            }
        }
    }
}
