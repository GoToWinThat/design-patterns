﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Protection_Proxy
{
    public class Driver
    {
        public int Age { get; set; }

        public Driver(int age)
        {
            Age = age;
        }
    }
}
