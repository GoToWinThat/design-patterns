﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonPattern.DI
{
    public class Foo
    {
        public EventBroker Broker;

        public Foo(EventBroker broker)
        {
            Broker = broker ?? throw new ArgumentNullException(paramName: nameof(broker));
        }
    }

    public class EventBroker
    {

    }
}
