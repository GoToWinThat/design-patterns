﻿using FactoryPattern.AbstractFactory.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.AbstractFactory.Drinks
{
    internal class Coffee : IHotDrink
    {
        public void Consume()
        {
            Console.WriteLine("This coffee is delicious!");
        }
    }
}
