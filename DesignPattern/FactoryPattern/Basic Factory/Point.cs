﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Basic_Factory
{
    public partial class Point
    {
        private double x, y;

        protected Point(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public override string ToString()
        {
            return $"{nameof(x)}: {x}, {nameof(y)}: {y}";
        }

        //Bad idea for creating point
        public Point(double a, double b, CoordinateSystem cs = CoordinateSystem.Cartesian)
        {
            switch (cs)
            {
                case CoordinateSystem.Polar:
                    x = a * Math.Cos(b);
                    y = a * Math.Sin(b);
                    break;
                default:
                    x = a;
                    y = b;
                    break;
            }
        }

        public static Point Origin => new Point(0, 0);

        public static Point Origin2 = new Point(0, 0);

        //Static method when our constructor is protected
        public static Point NewCartesianPoint(double x, double y)
        {
            return new Point(x, y);
        }

        public static Point NewPolarPoint(double rho, double theta)
        {
            return new Point(rho * Math.Cos(theta), rho * Math.Sin(theta));
        }

        public enum CoordinateSystem
        {
            Cartesian,
            Polar
        }

    }
}
