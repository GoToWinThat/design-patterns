﻿using MoreLinq;
using System;
using System.Collections.Generic;
using System.Text;

namespace AdapterPattern.Lazy_Adapter
{
    public class Demo
    {
        private static readonly List<VectorObject> vectorObjects = new List<VectorObject>
    {
      new VectorRectangle(1, 1, 10, 10),
      new VectorRectangle(3, 3, 6, 6)
    };

        // the interface we have
        public static void DrawPoint(Point p)
        {
            Console.Write(".");
        }

        public static void Main(string[] args)
        {
            int count = 0;
            for (int x1 = 397; x1 <= 400; ++x1)
                for (int y1 = 397; y1 <= 400; ++y1)
                    for (int x2 = 397; x2 <= 400; ++x2)
                        for (int y2 = 397; y2 <= 400; ++y2)
                        {
                            if (x1 == x2 && y1 == y2) continue;
                            var p1 = new Point(x1, y1);
                            var p2 = new Point(x2, y2);
                            if (p1.GetHashCode() == p2.GetHashCode())
                            {
                                //WriteLine($"{p1} hash == {p2} hash == {p1.GetHashCode()}");
                                count++;
                            }
                        }
            Console.WriteLine($"{count} collisions");
        }

        private static void Draw()
        {
            foreach (var vo in vectorObjects)
            {
                foreach (var line in vo)
                {
                    var adapter = new LineToPointAdapter(line);
                    adapter.ForEach(DrawPoint);
                }
            }
        }
    }
}
