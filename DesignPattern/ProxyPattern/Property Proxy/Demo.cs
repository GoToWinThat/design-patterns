﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Property_Proxy
{
    public class Demo
    {
        static void Main(string[] args)
        {
            var c = new Creature();
            c.Agility = 12; // c.set_agility()
                            // c.Agility = new Property<int>(10);
                            //c.agility = 12;
        }
    }
}
