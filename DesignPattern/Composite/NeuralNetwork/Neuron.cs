﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace CompositePattern.NeuralNetwork
{
    public class Neuron : IEnumerable<Neuron>
    {
        public List<Neuron> In, Out;

        public IEnumerator<Neuron> GetEnumerator()
        {
            yield return this;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }

        public void ConnectTo(Neuron other)
        {
            Out.Add(other);
            other.In.Add(this);
        }
    }
}
