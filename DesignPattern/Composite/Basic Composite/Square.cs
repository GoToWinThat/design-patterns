﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CompositePattern.Basic_Composite
{
    public class Square : GraphicObject
    {
        public override string Name => "Square";
    }
}
