﻿using FactoryPattern.Basic_Factory;
using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.Basic_Factory
{



    class PointFactory
    {
        public static Point NewCartesianPoint(float x, float y)
        {
        return new Point(x, y); // needs to be public
        }
    }
}
