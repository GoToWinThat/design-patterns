﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonPattern.Singleton
{
    public class MyDatabase
    {
        private MyDatabase()
        {
            Console.WriteLine("Initializing database");
        }
        private static Lazy<MyDatabase> instance =
          new Lazy<MyDatabase>(() => new MyDatabase());

        public static MyDatabase Instance => instance.Value;
    }
}
