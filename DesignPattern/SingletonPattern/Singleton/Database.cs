﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonPattern.Singleton
{
    public class Database
    {
        private Database()
        {
        }

        public static Database Instance { get; } = new Database();
    }
}
