﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPattern
{
    class HtmlBuilder
    {
        private readonly string rootName;
        protected HtmlElement root = new HtmlElement();

        public HtmlBuilder(string rootName)
        {
            this.rootName = rootName;
            root.Name = rootName;
        }

        // not fluent
        public void AddChild(string childName, string childText)
        {
            var e = new HtmlElement(childName, childText);
            root.Elements.Add(e);
        }
        // fluent
        public HtmlBuilder AddChildFluent(string childName, string childText)
        {
            var e = new HtmlElement(childName, childText);
            root.Elements.Add(e);
            return this;
        }

        public override string ToString() => root.ToString();

        public void Clear()
        {
            root = new HtmlElement { Name = rootName };
        }

        public HtmlElement Build() => root;


        //implicit operator - conversion operator, determines how HtmlElement will be created if you convert from HtmlBuilder
        //In this example we will take root - instance of HtmlElement 
        public static implicit operator HtmlElement(HtmlBuilder builder)
        {
            return builder.root;
        }
    }
}