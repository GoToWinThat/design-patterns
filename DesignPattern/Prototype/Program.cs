﻿using Prototype.Copy_Constructor;
using System;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
           /* var john = new Person("John", new Address("123 London Road", "London", "UK"));

            //var chris = john;
            var jane = new Person(john);

            jane.Name = "Jane";

            Console.WriteLine(john); // oops, john is called chris
            Console.WriteLine(jane);

            var main = new Person(null, new Address("123 East Dr", "London", 0));
            var aux = new Person(null, new Address("123B East Dr", "London", 0));
            */

            var john = new Prototype.Prototype_Factory.Person("John", new Prototype.Prototype_Factory.Address("123 London Road", "London", 123));

            //var chris = john;
            var jane = new Prototype.Prototype_Factory.Person(john);

            jane.Name = "Jane";

            Console.WriteLine(john); // oops, john is called chris
            Console.WriteLine(jane);


        }
    }
}
