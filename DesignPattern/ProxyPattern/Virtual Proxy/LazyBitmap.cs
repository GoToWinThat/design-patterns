﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Virtual_Proxy
{
    class LazyBitmap : IImage
    {
        private readonly string filename;
        private Bitmap bitmap;

        public LazyBitmap(string filename)
        {
            this.filename = filename;
        }


        public void Draw()
        {
            if (bitmap == null)
                bitmap = new Bitmap(filename);

            bitmap.Draw();
        }
    }
}
