﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Dynamic_Proxy
{
    public interface IBankAccount
    {
        void Deposit(int amount);
        bool Withdraw(int amount);
        string ToString();
    }
}
