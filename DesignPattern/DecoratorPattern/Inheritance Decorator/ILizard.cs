﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DecoratorPattern.Inheritance_Decorator
{
    public interface ILizard : ICreature
    {
        void Crawl();
    }
}
