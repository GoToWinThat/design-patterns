﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BridgePattern.Bridge
{
    public interface IRenderer
    {
        void RenderCircle(float radius);
    }
}
