﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SingletonPattern.Singleton
{
    public interface IDatabase
    {
        int GetPopulation(string name);
    }
}
