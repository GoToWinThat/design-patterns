﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace DecoratorPattern.Inheritance_Decorator
{
    public class Lizard : ILizard
    {
        public int Age { get; set; }
        public void Crawl()
        {
            if (Age < 10)
                WriteLine("I am crawling!");
        }
    }
}
