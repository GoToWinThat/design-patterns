﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ProxyPattern.Virtual_Proxy
{
    interface IImage
    {
        void Draw();
    }
}
