﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FactoryPattern.AbstractFactory.Interfaces
{
    public interface IHotDrink
    {
        void Consume();
    }
}
