﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ChainOfResposibilityPattern.Method_Chain
{
    public class Demo
    {
        static void Main(string[] args)
        {
            var goblin = new Creature("Goblin", 1, 1);
            WriteLine(goblin);

            var root = new CreatureModifier(goblin);

            //root.Add(new NoBonusesModifier(goblin));

            root.Add(new DoubleAttackModifier(goblin));
            root.Add(new DoubleAttackModifier(goblin));

            root.Add(new IncreaseDefenseModifier(goblin));

            // eventually...
            root.Handle();
            WriteLine(goblin);
        }
    }
}
