﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ProxyPattern.Virtual_Proxy
{
    class Demo
    {
        public static void DrawImage(IImage img)
        {
            WriteLine("About to draw the image");
            img.Draw();
            WriteLine("Done drawing the image");

        }

        static void Main()
        {
            var img = new LazyBitmap("pokemon.png");
            DrawImage(img);
        }
    }
}
