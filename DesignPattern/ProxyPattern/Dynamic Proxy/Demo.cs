﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ProxyPattern.Dynamic_Proxy
{
    public class Demo
    {
        static void Main(string[] args)
        {
            //var ba = new BankAccount();
            var ba = Log<BankAccount>.As<IBankAccount>();

            ba.Deposit(100);
            ba.Withdraw(50);

            WriteLine(ba);
        }
    }
}
