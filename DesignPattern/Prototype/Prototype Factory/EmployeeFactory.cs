﻿using Prototype.Serialization;
using System;
using System.Collections.Generic;
using System.Text;

namespace Prototype.Prototype_Factory
{
    public class EmployeeFactory
    {
        private static Person main =
          new Person(null, new Address("123 East Dr", "London", 0));
        private static Person aux =
          new Person(null, new Address("123B East Dr", "London", 0));

        private static Person NewEmployee(Person proto, string name, int suite)
        {
            var copy = proto.DeepCopy();
            copy.Name = name;
            copy.Address.Suite = suite;
            return copy;
        }

        public static Person NewMainOfficeEmployee(string name, int suite) =>
          NewEmployee(main, name, suite);

        public static Person NewAuxOfficeEmployee(string name, int suite) =>
          NewEmployee(aux, name, suite);
    }
}
