﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BuilderPattern.Inheritance_Builder
{
    public abstract class PersonBuilder
    {
        protected Person person = new Person();

        public Person Build()
        {
            return person;
        }
    }
    //CRTP Pattern 
    // It's tricky "loop" in generic class
    //We are using this when base class have to know type of child class for exmaple in fluent api or coping objects
    //So we are creating loop in generic type when generic must to be type of class itself (in base class) -> go to next class
    public class PersonInfoBuilder<SELF> : PersonBuilder  where SELF : PersonInfoBuilder<SELF>
    {
        public SELF Called(string name)
        {
            person.Name = name;
            return (SELF)this;
        }
    }
    //Now that class inherit from base class which is PersonInfoBuilder<SELF> but SELF is? PersonJobBuilder<SELF> and SELF is? like above PersonJobBuilder<SELF>
    //And how base class know about child type? Base class are getting generic type SELF which is type of child class PersonJobBuilder. IT's DONE :)
    public class PersonJobBuilder<SELF> : PersonInfoBuilder<SELF> where SELF : PersonJobBuilder<SELF>
    {
        public SELF WorksAsA(string position)
        {
            person.Position = position;
            return (SELF)this;
        }
    }
    //Even if you want go further you just inherit from class above and Base initial is:
    //PersonInfoBuilder<PersonJobBuilder<PersonBirthDateBuilder<SELF>>> - still have SELF loop with last Builder but its okey
    //What we get? Base class know about children types because of propagation of types which we get thanks to SELF generic type
    public class PersonBirthDateBuilder<SELF> : PersonJobBuilder<SELF> where SELF : PersonBirthDateBuilder<SELF>
    {
        public SELF Born(DateTime dateOfBirth)
        {
            person.DateOfBirth = dateOfBirth;
            return (SELF)this;
        }
    }
}
