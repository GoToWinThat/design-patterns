﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ProxyPattern.Protection_Proxy
{
    public class Car : ICar
    {
        public void Drive()
        {
            WriteLine("Car being driven");
        }
    }
}
