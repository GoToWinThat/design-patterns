﻿using BuilderPattern.Facets_Builder;
using BuilderPattern.Parameter_Builder;
using BuilderPattern.Inheritance_Builder;
using System;
using System.Text;

namespace BuilderPattern
{
    class Program
    {
        static void Main()
        {
            {
                // StringBuilder exaple
                var hello = "hello";
                var sb = new StringBuilder();
                sb.Append("<p>");
                sb.Append(hello);
                sb.Append("</p>");
                Console.WriteLine(sb);

                var words = new[] { "hello", "world" };
                sb.Clear();
                sb.Append("<ul>");
                foreach (var word in words)
                {
                    sb.AppendFormat("<li>{0}</li>", word);
                }

                sb.Append("</ul>");
                Console.WriteLine(sb);

                // Non-fluent builder
                var builder = new HtmlBuilder("ul");
                builder.AddChild("li", "hello");
                builder.AddChild("li", "world");
                Console.WriteLine(builder.ToString());

                // Fluent builder
                sb.Clear();
                builder.Clear(); 
                builder.AddChildFluent("li", "hello").AddChildFluent("li", "world"); //Now when you are adding child, method returns object itself
                Console.WriteLine(builder);
            }

            // Factory method - even when someone want to use HtmlElement class, its still builder work
            {
                var builder = HtmlElement.Create("ul");
                builder.AddChildFluent("li", "hello")
                  .AddChildFluent("li", "world");
                Console.WriteLine(builder);
            }

            // Implicit operator 
            {
                HtmlElement root = HtmlElement
                  .Create("ul")
                  .AddChildFluent("li", "hello")
                  .AddChildFluent("li", "world");
                Console.WriteLine(root);
            }

            // Multi Builders

            //Disadvantage here is that we broke open-closed principal  because PersonBuilder - base class - know about childclasses and its bad practise
            var pb = new BuilderPattern.Facets_Builder.PersonBuilder();
            BuilderPattern.Facets_Builder.Person person = pb
              .Lives
                .At("123 London Road")
                .In("London")
                .WithPostcode("SW12BC")
              .Works
                .At("Fabrikam")
                .AsA("Engineer")
                .Earning(123000);
            Console.WriteLine(person);


            //We are using function as param and now user need to use builder
            var ms = new MailService();
            ms.SendEmail(email => email.From("foo@bar.com")
                .To("bar@baz.com")
                .Body("Hello, how are you?"));

            // CRTP Fluent Api
            var me = BuilderPattern.Inheritance_Builder.Person.New
            .Called("Dmitri")
            .WorksAsA("Quant")
            .Born(DateTime.UtcNow)
            .Build();
            Console.WriteLine(me);
        }
    }
}
