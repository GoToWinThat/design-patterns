﻿using System;
using System.Collections.Generic;
using System.Text;
using static System.Console;

namespace ChainOfResposibilityPattern.Method_Chain
{
    public class NoBonusesModifier : CreatureModifier
    {
        public NoBonusesModifier(Creature creature)
          : base(creature) { }

        public override void Handle()
        {
            // nothing
            WriteLine("No bonuses for you!");
        }
    }
}
